# wg-docker-template

1. Clone git repository
```
git clone https://gitlab.com/strategiczone/wg-docker-template.git
```
2. Create DNS record
```
dns-rec my-dns.domaine.com
```
3. Change port and DOMAIN_NAME
In `.env` change `SERVICE`, `WG_PORT`, `DOMAIN_NAME`, `SUBSPACE_IPV4_POOL` and `SUBSPACE_IPV6_POOL`
